$(document).ready(function () {
  $("div#no_provider_found").hide();
  /* filter idps as you type */
  $('#idp_search_input').keyup(function () {
    if (this.value.length > 0) {
      $('div.idp_list a').hide().filter(function () {
        return $(this).text().toLowerCase().indexOf($('#idp_search_input').val().toLowerCase()) != -1;
      }).show();
      $n = $('a.entity').filter(":visible").length;
      if ($n == 0) {
        $("div#no_provider_found").show();
      }
      else {
        $("div#no_provider_found").hide();
      }
    }
  });
});