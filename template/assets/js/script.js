/* Function to get URL parameter */
function GetURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('&');
  for (var i = 0; i < sURLVariables.length; i++) {
    var sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    }
  }
}

/* Function to hold message contents */
function GetMessageBody(messageAction) {
  var messages = [];
  messages[0] = "Reset your password.";
  messages[1] = "Your account has been created.";
  messages[2] = "Your password has been reset.";
  return messages[messageAction];
}


/* Function to set the CSS for the message container */
function SetMessageCSS(messageAction) {
  var classes = [];
    classes[0] = "alert-danger";
    classes[1] = "alert-success";
    classes[2] = "alert-success";
  $("#messages").removeClass("alert-danger");
  $("#messages").removeClass("alert-success");
  $("#messages").addClass(classes[messageAction]);
}


/* Function to pupulate messages container */
function UpdateMessages() {
  var messageAction = GetURLParameter('action');
  if (!messageAction) messageAction = 0;
  var messageBody = GetMessageBody(messageAction);
  SetMessageCSS(messageAction);
  $("#messages").html(messageBody);
  $("#messages").show();
  console.log(messageAction);
}


$(document).ready(function () { 
  /* Set the script in motion when page is ready */
  UpdateMessages();
});